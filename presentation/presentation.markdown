---
title: De novo Assembly
author: Tilman Schell, Malte Petersen
---

# Introduction

## Data types

### Short read (SR)

- Platforms: Illumina, BGISeq
- Considerations: insert size, MP/PE

### Long read (LR)

- Platforms: PB, ONP
- Considerations: fresh tissue, read length distribution

## Assembly types

- SR -> scaffold (MP/LR) -> gap close (SR/LR)
- Hybrid
- Manual: LR -> polish

# Information from k-mers

## k-mer counting with Jellyfish

What can you glean from the histogram?

- Best k with kmergenie
- Genome size
- Heterozygosity

Show examples of histograms

Discuss tradeoff between longer k vs shorter k

# De Bruijn graph assembler

## Explain general algorithm

Material from Stefan

Advantages and disadvantages

Examples

## DIY

# Assembly statistics

With QUAST + BUSCO

- Contiguity
- Completeness
- Accuracy

Which mapper do we use?
